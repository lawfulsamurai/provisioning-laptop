#! /bin/bash

echo -e "\n\n>>> Installing required packages"
sudo apt-get install -y \
    git \
    python-dev \
    libxml2-dev \
    libxslt-dev \
    libssl-dev \
    curl \
    python-setuptools \
    python-pip

# Create ~/setup.
# All subsequent operations will execute in ~/setup.
echo -e "\n\n>>> Creating ~/setup"
pushd ~ && mkdir -p setup && pushd setup

# Clone the provisioning repo here.
echo -e "\n\n>>> Cloning the provisioning repo."
git clone https://lawfulsamurai@bitbucket.org/lawfulsamurai/provisioning-laptop.git

# Switch to the git repo directory.
echo -e "\n\n>>> Switching to the provisioning repo."
pushd provisioning-laptop

# Install virtualenvwrapper.
echo -e "\n\n>>> Installing virtualenvwrapper"
sudo pip install virtualenvwrapper

# Setup some aliases to make working with virtualenvwrapper easier.
echo -e "\n\n>>> Setting up virtualenvwrapper aliases"
export WORKON_HOME=$HOME/.virtualenvs
export PROJECT_HOME=$HOME/Devel
source /usr/local/bin/virtualenvwrapper.sh

# Create a virtual environment for provisioning.
echo -e "\n\n>>> Setting up environment for provisioning"
mkvirtualenv provisioning

echo -e "\n\n>>> Installing ansible"
pip install -r requirements.txt

echo -e "\n\n>>> Provision using ansible"
ansible-playbook setup.yml -i hosts --ask-sudo-pass
